class CreateUrls < ActiveRecord::Migration[7.1]
  def change
    create_table :urls do |t|
      t.string :url, null: false
      t.string :token, null: false, index: { unique: true }
      t.integer :counter, null: false, default: 0

      t.timestamps
    end
  end
end
