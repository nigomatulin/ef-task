# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Urls' do
  describe 'POST /urls' do
    context 'with valid params' do
      let(:params) {
        {
          url: Faker::Internet.url
        }
      }

      it 'returns http success' do
        post urls_url, params: params
        expect(response).to have_http_status(:success)
        expect(response.body).to be_json.with_content(short_url: proc { true })
      end

      it 'creates a new short url' do
        expect {
          post urls_url, params: params
        }.to change(Url, :count)
      end
    end

    context 'with invalid params' do
      let(:params) {
        {
          url: 'foobar'
        }
      }

      it 'returns http success' do
        post urls_url, params: params
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.body).to be_json.with_content({ 'success' => false })
      end

      it 'creates a new short url' do
        expect {
          post urls_url, params: params
        }.not_to change(Url, :count)
      end
    end

    context 'without params' do
      it 'returns http success' do
        post urls_url
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.body).to be_json.with_content({ 'success' => false })
      end

      it 'creates a new short url' do
        expect {
          post urls_url
        }.not_to change(Url, :count)
      end
    end
  end

  describe 'GET /urls/:short_url' do
    let(:url) { create(:url) }

    it 'correct params' do
      init_counter = url.counter
      get url_url(url.token)
      expect(response).to have_http_status(:success)
      expect(response.body).to be_json.with_content({ 'url' => url.url })
      url.reload
      expect(url.counter).to equal(init_counter + 1)
    end

    it 'incorrect params' do
      get url_url('shrt')
      expect(response).to have_http_status(:not_found)
      expect(response.body).to be_json.with_content({ 'success' => false })
    end

    it 'without params' do
      get '/urls'
      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'GET /urls/:short_url/stats' do
    let(:url) { create(:url) }

    it 'correct params' do
      get stats_url_url(url.token)
      expect(response).to have_http_status(:success)
      expect(response.body).to be_json.with_content({ 'counter' => url.counter })
    end

    it 'incorrect params' do
      get url_url('shrt')
      expect(response).to have_http_status(:not_found)
      expect(response.body).to be_json.with_content({ 'success' => false })
    end
  end
end
