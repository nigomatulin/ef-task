# frozen_string_literal: true

# == Schema Information
#
# Table name: short_urls
#
#  id         :integer          not null, primary key
#  counter    :integer          default(0), not null
#  token      :string           not null
#  url        :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_short_urls_on_token  (token) UNIQUE
#
FactoryBot.define do
  factory :url do
    url { Faker::Internet.url }
  end
end
