# frozen_string_literal: true

# == Schema Information
#
# Table name: short_urls
#
#  id         :integer          not null, primary key
#  counter    :integer          default(0), not null
#  token      :string           not null
#  url        :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_short_urls_on_token  (token) UNIQUE
#
require 'rails_helper'

RSpec.describe Url do
  describe 'validations' do
    subject { create(:url) }

    it { is_expected.to validate_presence_of(:url) }
    it { is_expected.to validate_uniqueness_of(:token) }
    it { is_expected.to validate_url_of(:url) }
  end
end
