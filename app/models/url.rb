# frozen_string_literal: true

# == Schema Information
#
# Table name: urls
#
#  id         :integer          not null, primary key
#  counter    :integer          default(0), not null
#  token      :string           not null
#  url        :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_urls_on_token  (token) UNIQUE
#
class Url < ApplicationRecord
  TOKEN_LENGTH = 5

  before_validation -> { self.token ||= generate_token }

  validates :url, presence: true, url: true
  validates :token, uniqueness: { case_sensitive: true }

  def short_url
    Rails.application.routes.url_helpers.url_url(self.token)
  end

  private

  def generate_token
    loop do
      candidate = SecureRandom.alphanumeric(TOKEN_LENGTH)
      return candidate unless Url.exists?(token: candidate)
    end
  end
end
