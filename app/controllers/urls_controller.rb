# frozen_string_literal: true

class UrlsController < ApplicationController
  before_action :get_url, only: %i[show stats]

  def show
    @url.update(counter: @url.counter + 1)
    render json: @url.to_json(only: :url)
  end

  def create
    @url = Url.create(url_params)
    if @url.valid?
      render json: @url.to_json(only: [], methods: :short_url)
    else
      render json: { success: false }, status: :unprocessable_entity
    end
  end

  def stats
    render json: @url.to_json(only: :counter)
  end

  private

  def get_url
    @url = Url.find_by(token: params[:short_url])
    render json: { success: false }, status: :not_found unless @url
  end

  def url_params
    params.permit(:url)
  end
end
